package org.eclipse.test;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.*;

import org.eclipse.main.Calcul;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class CalculTest {

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
	}

	@AfterEach
	void tearDown() throws Exception {
	}


	@Test
	void testSomme() {
		Calcul calcul = new Calcul();
		
		assertFalse("2 entiers positifs", calcul.somme(2, 3) == 0);
//		if(calcul.somme(2, 3) != 5)
//			fail("faux pour deux entiers positifs");
		if(calcul.somme(-2, -3) != -5)
			fail("faux pour deux entiers n�gatifs");
		if(calcul.somme(-2, 3) !=1)
			fail("faux pour deux entiers de signe diff�rent");
		if(calcul.somme(0, 3) != 3)
			fail("faux pour x nul");
		if(calcul.somme(2, 0) !=2)
			fail("faux pour y nul");
		if(calcul.somme(0, 0) !=0)
			fail("faux pour x et y nuls");
		
	}
	@Test
	void testDivision() {
		
		Calcul calcul = new Calcul();
		assertFalse("2 entiers positifs", calcul.division(6, 3) == 0);
		assertEquals("2 entiers n�gatifs", 2 , calcul.division(-6, -3));
		assertNotNull("2 entiers de signe diff�rent", calcul.division(-6, 3));
		assertTrue("entier x nul",calcul.division(0,3) == 0);
		
		Throwable e = null;		
		try {
			calcul.division(2, 0);
		}
		catch(Throwable ex) {
			e = ex;
		}
		assertTrue(e instanceof ArithmeticException);
		e = null;
		
		try {
			calcul.division(0, 0);
		}
		catch(Throwable ex){
			e = ex;
		}
		assertTrue (e instanceof ArithmeticException);
	}
	@Test	
	void testSoustraction() {
		
		Calcul calcul = new Calcul();
		if(calcul.soustraction(2, 3) != -1)
			fail("faux pour deux entiers positifs");
		if(calcul.soustraction(-2, -3) != 1)
			fail("faux pour deux entiers n�gatifs");
		if(calcul.soustraction(-2, 3) != -5)
			fail("faux pour deux entiers de signe diff�rent");
		if(calcul.soustraction(0, 3) != -3)
			fail("faux pour x nul");
		if(calcul.soustraction(2, 0) != 2)
			fail("faux pour y nul");
		if(calcul.soustraction(0, 0) !=0)
			fail("faux pour x et y nuls");		
	}
	
	@Test	
	void testProduit() {
		Calcul calcul = new Calcul();
		assertTrue("entiers x et y nuls", calcul.produit(0, 0) == 0);
		assertEquals("2 entiers n�gatifs",3,calcul.produit(-1, -3));
		assertFalse("2 entiers de signe different", calcul.produit(1, -3) == 3);
		assertNotNull("1 entier nul", calcul.produit(0, 3));
	}
}

